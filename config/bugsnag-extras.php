<?php

return [
    'cluster' => [
        'name' => env('CLUSTER_NAME'),
        'pool_name' => env('CLUSTER_POOL_NAME'),
        'node_name' => env('CLUSTER_NODE_NAME'),
    ],
];
