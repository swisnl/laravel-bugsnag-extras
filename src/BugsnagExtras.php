<?php

namespace Swis\Laravel\BugsnagExtras;

use Bugsnag\Configuration;

class BugsnagExtras
{
    public function alterConfiguration(Configuration $config): Configuration
    {
        return $config->mergeDeviceData(['cluster' => $this->getCluster()]);
    }

    protected function getCluster(): array
    {
        return array_filter([
            'name' => config('bugsnag-extras.cluster.name'),
            'poolName' => config('bugsnag-extras.cluster.pool_name'),
            'nodeName' => config('bugsnag-extras.cluster.node_name'),
        ]);
    }
}
