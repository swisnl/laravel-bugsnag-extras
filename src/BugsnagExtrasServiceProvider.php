<?php

namespace Swis\Laravel\BugsnagExtras;

use Bugsnag\Client;
use Illuminate\Contracts\Foundation\Application;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class BugsnagExtrasServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('laravel-bugsnag-extras')
            ->hasConfigFile();
    }

    public function packageBooted(): void
    {
        $this->app->afterResolving('bugsnag', function (Client $client, Application $app) {
            /** @var \Swis\Laravel\BugsnagExtras\BugsnagExtras $extras */
            $extras = $app->make(BugsnagExtras::class);
            $extras->alterConfiguration($client->getConfig());
        });
    }
}
